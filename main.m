tic;

addpath('Common');
addpath('Combination');
addpath('EuclidieanDistanceClassifier');
addpath('KNNClassifier');
addpath('Resources');

% Train dataset
train_file = "app.tif";
train_img = imread(train_file);
train_examples_per_digit = 20;
rectangle_cells_train = extract_digit(train_img, train_examples_per_digit);

% Test dataset
test_file = "test.tif";
test_img = imread(test_file);
test_examples_per_digit = 10;
rectangle_cells_test = extract_digit(test_img, test_examples_per_digit);

% Train & Test every classifier
train_test_euclidean_dist_classifier(train_img,rectangle_cells_train, test_img,rectangle_cells_test,train_examples_per_digit, test_examples_per_digit);

n = 8; 
m = 6;
K = 3;

train_test_KNN_classifier(train_img, rectangle_cells_train, test_img, rectangle_cells_test, train_examples_per_digit, test_examples_per_digit, n, m, K);

% Get the probability vectors computed by each classifier
probabili_KNN = readmatrix("probabilities_KNN");
probabiliti_eucli = readmatrix("probabilities_eucli");

% Combine both classifiers with SUM
combinaison_somme = combinaison_class_somme(probabili_KNN, probabiliti_eucli);
combinaison_prod = combinaison_class_prod(probabili_KNN, probabiliti_eucli);

% test the predictions
[ratio_sum, predict_sum] = test_predictions(combinaison_somme, test_examples_per_digit);
fprintf("when KNN and Enclidiean Distance combined with SUM: %d\n\n", ratio_sum);
show_results(test_img, predict_sum, rectangle_cells_test, test_examples_per_digit, 'Sum combination');

[ratio_prod, predict_prod] = test_predictions(combinaison_prod, test_examples_per_digit);
fprintf("when KNN and Enclidiean Distance combined with PROD: %d\n\n", ratio_prod);
show_results(test_img, predict_prod, rectangle_cells_test, test_examples_per_digit, 'Prod combination');

delete *.dat;

toc;