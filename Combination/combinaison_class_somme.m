function [out] = combinaison_class_somme(tabP1,tabP2)
    out = (tabP1 + tabP2) ./ sum(tabP1 + tabP2, 1);
end

