function show_results(img,predictions, rectangle_cells, examples_per_class, titl)

    nb_digits = examples_per_class * 10;
    text_str = cell(nb_digits, 1);
    positions = zeros(nb_digits, 2);
    box_color = cell(1,nb_digits);
    
    for i = 1:nb_digits
        text_str{i} = int2str(predictions(i));
        positions(i,1) = rectangle_cells(i, 3) + (rectangle_cells(i, 4) - rectangle_cells(i, 3))/2;
        positions(i,2) = rectangle_cells(i, 2);
        box_color{i} = 'red';
    end
    
    img_res = insertText(img,positions,text_str,'FontSize',20,'BoxColor',box_color,'BoxOpacity',0.4,'TextColor','white');
    figure
    imshow(img_res);
    title(titl);
end

