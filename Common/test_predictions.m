function [ratio, predict] = test_predictions(predictions, test_examples_per_digit)
    predict = zeros(1, 100);
    for i = 1:1:100
        [val, predict(i)] = max(predictions(:, i));
    end
    predict = predict - 1;

    good_predict = get_class_per_digit(test_examples_per_digit);
    good_predict = rot90(good_predict);

    ratio = 0;
    for i = 1:1:100
        if good_predict(i) ~= predict(i)
            ratio = ratio + 1;
        end
    end
end