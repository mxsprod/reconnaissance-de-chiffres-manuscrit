function [rectangle_cells] = extract_digit(src_img, nb_examples_per_digit)
% Find every digit from src_img and return there bounding rectangles (rectangle_cells).
% Each line of rectangle_cells follows the same pattern : (x_inf, x_sup, y_inf, y_sup). 

    % binarize the image
    src_img_bw = 1 - imbinarize(src_img);
    size_src = size(src_img);
    
    [rows,cols] = size(src_img);
    X = 1:rows;
    Y = 1:cols;
    
    % compute horizontal projection
    horizontalProj = sum(src_img_bw,2);
    
    % find all local minimums in the horizontal projection to determine
    % space between digit lines
    ML = islocalmin(horizontalProj, 'FlatSelection','center');
    
    pzerox = X(ML);
    pzerox(horizontalProj(pzerox)>0) = [];
    pzeroh = zeros([1,11]);
    
    pzerox = [1, pzerox, size_src(1)];
    pzeroy = zeros([1, 21]);
    
    rectangle_cells = zeros([nb_examples_per_digit * 10, 4]);
    rectangle_cells_i = 1;
    
    for i = 1:1:10
        clear pzeroy;
        
        % take only the right digit line
        part = src_img_bw(pzerox(i):pzerox(i+1), :);
        
        % compute vertical projection
        verticalProj = sum(part, 1);
            
        % find all local minimums in the vertical projection to determine
        % horizontal space between digit
        ML = islocalmin(verticalProj, 'FlatSelection','center');
        
        pzeroy = Y(ML);
        pzeroy(verticalProj(pzeroy)>0) = [];
        pzerov = zeros([1,21]);
        pzeroy = [1, pzeroy, size_src(2)];
        
        for n = 1:1:nb_examples_per_digit
            local_pzerox = pzerox;
            local_pzeroy = pzeroy;
            mat = src_img_bw(pzerox(i):pzerox(i+1), pzeroy(n):pzeroy(n+1));
            
            % Crop rectangle on x
            j = 1;
            while mat(j, :) == zeros(pzerox(i+1) - pzerox(i) + 1, 1)
                j = j + 1;
            end
            local_pzerox(i) = pzerox(i) + j;
            j = 1;
            while mat(pzerox(i+1) - pzerox(i) + 1 - j, :) == zeros(pzerox(i+1) - pzerox(i) + 1, 1)
                j = j + 1;
            end
            local_pzerox(i+1) = pzerox(i+1) - j; 
            
            % Crop rectangle on y
            j = 1;
            while mat(:, j) == zeros(1, pzeroy(n+1) - pzeroy(n) + 1, 1)
                j = j + 1;
            end
            local_pzeroy(n) = pzeroy(n) + j;
            j = 1;
            while mat(:, pzeroy(n+1) - pzeroy(n) + 1 - j) == zeros(1, pzeroy(n+1) - pzeroy(n) + 1)
                j = j + 1;
            end
            local_pzeroy(n+1) = pzeroy(n+1) - j; 
    
            % create the rectangle with the left up corner and right bottom
            % corner coordinates
            rectangle_cells(rectangle_cells_i, :) = [local_pzerox(i), local_pzerox(i+1), local_pzeroy(n), local_pzeroy(n+1)];
            
            rectangle_cells_i = rectangle_cells_i + 1;
        end
    end
    
%     figure('Name', 'Result');
%     imshow(src_img);
%     for i = 1:1:(nb_examples_per_digit * 10)
%        hold on;
%        rectangle('Position',[rectangle_cells(i, 3),rectangle_cells(i, 1),rectangle_cells(i, 4) - rectangle_cells(i, 3),rectangle_cells(i, 2) - rectangle_cells(i, 1)], 'LineWidth', 1, 'LineStyle', '-')
%     end
    
end