function [class_per_digit] = get_class_per_digit(examples_per_digit)  
    class_per_digit = zeros(examples_per_digit * 10, 1);
    curr_digit = 0;
    for i = 1:1:examples_per_digit * 10
        class_per_digit(i, 1) = curr_digit;    

        if mod(i, examples_per_digit) == 0 && i > 0
            curr_digit = curr_digit + 1;
        end 
    end
end