function [rectangle_densities] = get_zoning_densities(src_img, rectangle_cells, n, m) 
% Cut each digit of the image in n * m zones, 
% then compute each zone's pixel density. 

    src_img_bw = 1 - imbinarize(src_img); % binarize the image
    size_rectangle_cells = size(rectangle_cells);
    total_zones = n * m;
    rectangle_densities = zeros(size_rectangle_cells(1), total_zones); 
    
    % for each digit in the image
    for i = 1:1:size_rectangle_cells(1)
        
        % get the right digit coordinate / rectangle
        rectangle = rectangle_cells(i, :);
        
        zoning_densities = zeros(1, total_zones); 
        zone_index = 1;
    
        % extract coordinates from rectangle
        x_inf = rectangle(1);
        x_sup = rectangle(2);
        y_inf = rectangle(3);
        y_sup = rectangle(4);
        
        pas_x = ceil((x_sup - x_inf) / n);
        pas_y = ceil((y_sup - y_inf) / m);
        
        prev_x = x_inf;
        prev_y = y_inf;
        
        
        % cut incrementaly into n * m zones
        for x = (x_inf + pas_x):pas_x:x_sup
            for y = (y_inf + pas_y):pas_y:y_sup
                
                % return to the begin of the digit line
                if prev_y > y 
                    prev_y = y_inf;
                end
                
                % get the right zone from the image
                zone = src_img_bw(prev_x:x, prev_y:y);
                size_zone = size(zone);
                
                % compute white px densities
                total_whitepx = sum(zone, 'all');
                px_white_density = total_whitepx / (size_zone(1) * size_zone(2));
                zoning_densities(1, zone_index) = px_white_density;
                zone_index = zone_index + 1;
                
                prev_y = y;
            end
            
            prev_x = x;
        end
        
        % add densitites of ième digit
        rectangle_densities(i, :) = zoning_densities; 
    end
end