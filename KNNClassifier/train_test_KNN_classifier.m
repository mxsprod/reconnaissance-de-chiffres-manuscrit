function train_test_KNN_classifier(train_img, train_rectangles, test_img, test_rectangles, train_examples_per_digit, test_examples_per_digit, n, m, K)   
    train_class_per_digit = get_class_per_digit(train_examples_per_digit);
    % Training
    train_densities = get_zoning_densities(train_img, train_rectangles, n, m); % i.e caracteristics vector
    writematrix(train_densities, 'train_densities_KNN.dat');

    % Testing
    test_densities = get_zoning_densities(test_img, test_rectangles, n, m); % i.e caracteristics vector

    % compute probabilities
    probabilities = KNN_classifier(train_densities, test_densities, train_class_per_digit, K);
    writematrix(probabilities, 'probabilities_KNN.dat');
    
    % test the predictions
    [ratio, predict] = test_predictions(probabilities, test_examples_per_digit);
    fprintf("Ratio for KNN Classifier: %d\n\n", ratio);
    show_results(test_img, predict, test_rectangles, test_examples_per_digit, 'KNN');
end