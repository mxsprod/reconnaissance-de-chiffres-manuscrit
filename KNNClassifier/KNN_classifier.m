function [probabilities] = KNN_classifier(train_densities, test_densities, train_class_per_digit, K)
% Compute the probability vector using a KNN Classifier.

    probabilities = zeros(size(test_densities, 1), 10);

    for index_dens = 1:1:size(test_densities, 1)
        test_density = test_densities(index_dens,:);
        Idx = knnsearch(train_densities, test_density, 'K', K);
        knn_classes = train_class_per_digit(Idx, :);
        total = zeros(1,10);

        for index = 1:1:size(knn_classes, 1)
            class = knn_classes(index);
            total(class + 1) = total(class + 1) + 1;
        end

        probabilities(index_dens, :) = total / size(knn_classes, 1);
    end
    
    probabilities = probabilities';
end