% Load the image
src_img_col = imread("test.tif");
src_img = 1 - imbinarize(src_img_col);
size_src = size(src_img);

[rows,cols] = size(src_img);
X = 1:rows;
Y = 1:cols;

% Projection horizontale
horizontalProj = sum(src_img,2);
ML = islocalmin(horizontalProj, 'FlatSelection','center');

pzerox = X(ML);
pzerox(horizontalProj(pzerox)>0) = [];
pzeroh = zeros([1,11]);

pzerox = [1, pzerox, size_src(1)];
pzeroy = zeros([1, 21]);

rectangle_cells = zeros([200, 4]);
rectangle_cells_i = 1;

for i = 1:1:10
    clear pzeroy;
    
    part = src_img(pzerox(i):pzerox(i+1), :);
    
    % Projection verticale
    verticalProj = sum(part, 1);
    ML = islocalmin(verticalProj, 'FlatSelection','center');
    
    pzeroy = Y(ML);
    pzeroy(verticalProj(pzeroy)>0) = [];
    pzerov = zeros([1,21]);
    
    pzeroy = [1, pzeroy, size_src(2)];
    %subplot(10, 1, i);
    %plot(Y, verticalProj, 'b-', pzeroy, pzerov, 'g*');  
    for n = 1:1:20
        local_pzerox = pzerox;
        local_pzeroy = pzeroy;
        mat = src_img(pzerox(i):pzerox(i+1), pzeroy(n):pzeroy(n+1));
        
        %Crop rectangle on x
        j = 1;
        while mat(j, :) == zeros(pzerox(i+1) - pzerox(i) + 1, 1)
            j = j + 1;
        end
        local_pzerox(i) = pzerox(i) + j;
        j = 1;
        while mat(pzerox(i+1) - pzerox(i) + 1 - j, :) == zeros(pzerox(i+1) - pzerox(i) + 1, 1)
            j = j + 1;
        end
        local_pzerox(i+1) = pzerox(i+1) - j; 
        
        %Crop rectangle on y
        j = 1;
        while mat(:, j) == zeros(1, pzeroy(n+1) - pzeroy(n) + 1, 1)
            j = j + 1;
        end
        local_pzeroy(n) = pzeroy(n) + j;
        j = 1;
        while mat(:, pzeroy(n+1) - pzeroy(n) + 1 - j) == zeros(1, pzeroy(n+1) - pzeroy(n) + 1)
            j = j + 1;
        end
        local_pzeroy(n+1) = pzeroy(n+1) - j; 

        rectangle_cells(rectangle_cells_i, :) = [local_pzerox(i), local_pzerox(i+1), local_pzeroy(n), local_pzeroy(n+1)];
        rectangle_cells_i = rectangle_cells_i + 1;
    end
end

figure('Name', 'Result');
imshow(src_img_col);
for i = 1:1:200
    hold on;
    rectangle('Position',[rectangle_cells(i, 3),rectangle_cells(i, 1),rectangle_cells(i, 4) - rectangle_cells(i, 3),rectangle_cells(i, 2) - rectangle_cells(i, 1)], 'LineWidth', 1, 'LineStyle', '-')
end