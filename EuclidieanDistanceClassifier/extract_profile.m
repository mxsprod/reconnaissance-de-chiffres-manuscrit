function [outVec] = extract_profile(xg, xd, yg, yd,src_img_col)
    nb_composantes = 20;
    src_img = 1 - imbinarize(src_img_col);
    
    image_part = src_img(xg:xd, yg:yd);
    
    size_image_part = size(image_part);
    height = size_image_part(1);
    width = size_image_part(2);
    
    levels = zeros(nb_composantes, 1);
    levels(1) = 1;
    for i = 1:1:nb_composantes-1
        levels(i+1) = floor(i * height / (nb_composantes-1));
    end
    
    %Profil gauche
    profil_gauche = zeros(nb_composantes, 1);
    for i = 1:1:nb_composantes
        while image_part(levels(i), profil_gauche(i) + 1) == 0
            profil_gauche(i) = profil_gauche(i) + 1;
        end
    end
    
    %Profil droit
    profil_droit = zeros(nb_composantes, 1);
    for i = 1:1:nb_composantes
        while image_part(levels(i), width-profil_droit(i)) == 0
            profil_droit(i) = profil_droit(i) + 1;
        end
    end
    
    %imshow(image_part);
    
    outVec = [profil_gauche / width; profil_droit / width];
end

