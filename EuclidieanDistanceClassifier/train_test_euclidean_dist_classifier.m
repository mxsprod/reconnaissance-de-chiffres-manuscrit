function train_test_euclidean_dist_classifier(app_image, rectangle_cells_train, test_image, rectangle_cells_test, train_examples_per_digit, test_examples_per_digit)

    class_distance = classifieur_distance_eucli(rectangle_cells_train, app_image, train_examples_per_digit);
    writematrix(class_distance, "train_densities_eucli.dat");
    
    out = decision_distance_eucli(class_distance, rectangle_cells_test, test_image);
    writematrix(out, "probabilities_eucli.dat");
    
    % test the predictions
    [ratio, predict] = test_predictions(out, test_examples_per_digit);
    fprintf("Ratio for Euclidiean Distance Classifier: %d\n\n", ratio);
    show_results(test_image, predict, rectangle_cells_test, test_examples_per_digit, 'Euclidian distance');
end