function [P1] = decision_distance_eucli(profiles, rectangle_cells, src_img)
    %DECISION1 Summary of this function goes here
    %   Detailed explanation goes here
    sizeRC = size(rectangle_cells);
    P1 = zeros(10, sizeRC(1));
    %digit_profile = extract_profile(rectangle_cells(1, 1), rectangle_cells(1, 2), rectangle_cells(1, 3), rectangle_cells(1, 4), "test.tif")
    for n = 1:1:sizeRC(1)
        digit_profile = extract_profile(rectangle_cells(n, 1), rectangle_cells(n, 2), rectangle_cells(n, 3), rectangle_cells(n, 4), src_img);
        sum_vec = 0;
        for j = 1:1:10
            sum_vec = sum_vec + exp(-dist(digit_profile, profiles(:,j)));
        end
        for i = 1:1:10
            P1(i, n) = exp(-dist(digit_profile, profiles(:,i)))/sum_vec;
        end
    end
end

