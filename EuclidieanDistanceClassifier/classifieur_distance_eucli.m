function [test] = classifieur_distance_eucli(rectangle_cells, src_img, nb_in_class)
    rectangle_cells_size = size(rectangle_cells);
    digit_profile = zeros(40, 10);
    for i = 1:1:10
        for n = 1:1:nb_in_class
            digit_profile(:, i) = digit_profile(:, i) + extract_profile(rectangle_cells(20 * (i-1) + n, 1), rectangle_cells(20 * (i-1) + n, 2), rectangle_cells(20 * (i-1) + n, 3), rectangle_cells(20 * (i-1) + n, 4), src_img);
        end
    end
    test = digit_profile / 20;
end

