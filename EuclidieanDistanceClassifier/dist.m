function [out] = dist(vec1, vec2)
    if size(vec1) ~= size(vec2) 
        error("Distance can't be calculated : Vecotrs must have same size!");
    end
    out = norm(abs(vec2 - vec1));
end

